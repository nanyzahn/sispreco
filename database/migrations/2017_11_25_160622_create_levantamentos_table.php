<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLevantamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('levantamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produto');
            $table->integer('produtor');
            $table->integer('municipio');
            $table->integer('preco')->unsigned();
            $table->integer('semana')->unsigned();
            $table->integer('ano')->unsigned();

            $table->unique(['produto', 'produtor', 'semana', 'ano']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('levantamentos');
    }
}
