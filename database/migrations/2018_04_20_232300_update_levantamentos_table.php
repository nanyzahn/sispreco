<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateLevantamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('levantamentos', function (Blueprint $table) {
            $table->integer('mes')->unsigned();

            $table->dropUnique(['produto', 'produtor', 'semana', 'ano']);
            $table->unique(['produto', 'produtor', 'semana', 'mes', 'ano']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('levantamentos', function (Blueprint $table) {
            $table->dropUnique(['produto', 'produtor', 'semana', 'mes', 'ano']);
            $table->unique(['produto', 'produtor', 'semana', 'ano']);

            $table->removeColumn('mes');
        });
    }
}
