<?php

/*
|--------------------------------------------------------------------------
| Sispreço API Routes
|--------------------------------------------------------------------------
*/

Route::get('/produtos', 'ProdutoController@get');

Route::get('/municipios', 'MunicipioController@get');

Route::get('/produtores/municipio/{id}', 'BeneficiarioController@get');

Route::get('/levantamento', 'LevantamentoController@find');
Route::post('/levantamento', 'LevantamentoController@create');
Route::get('/levantamento/anos', 'LevantamentoController@getAnosLevantamento');
Route::get('/levantamento/produtos', 'LevantamentoController@getProdutos');
Route::post('/levantamento/produtos', 'LevantamentoController@postProdutos');
Route::get('/levantamento/intervalo', 'LevantamentoController@getIntervaloPreco');
Route::post('/levantamento/intervalo', 'LevantamentoController@postIntervaloPreco');

Route::post('/importar/pevs', 'ImportarController@pevs');
Route::post('/importar/ppm', 'ImportarController@ppm');
Route::post('/importar/lspa', 'ImportarController@lspa');

Route::post('/relatorio/gerar', 'RelatorioController@gerar');
