<?php

namespace App\Models;

use App\Facades\Incaper;
use Illuminate\Support\Facades\Cache;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Municipio
 *
 * @property int $id_municipio
 * @property int $id_unidade_federativa_FK
 * @property string $nm_municipio
 * @property int $cd_codigo_ibge
 *
 * @property \App\Models\UnidadeFederativa $unidade_federativa
 * @property \Illuminate\Database\Eloquent\Collection $beneficiarios
 *
 * @package App\Models
 */
class Municipio extends Eloquent
{
    protected $connection = 'incaper';
    protected $table = 'municipios';
    protected $primaryKey = 'id_municipio';
    public $timestamps = false;


    protected $casts = [
        'id_unidade_federativa_FK' => 'int',
        'cd_codigo_ibge' => 'int',
    ];

    protected $fillable = [
        'id_unidade_federativa_FK',
        'nm_municipio',
        'cd_codigo_ibge',
    ];

    public function unidade_federativa()
    {
        return $this->belongsTo(\App\Models\UnidadeFederativa::class, 'id_unidade_federativa_FK');
    }

    public function beneficiarios()
    {
        return $this->hasMany(\App\Models\Beneficiario::class, 'id_municipio_FK');
    }

    public static function getAll()
    {
        return static::getFromIncaper();
    }

    /**
     * Retorna os municípios da API do Incaper.
     *
     * @return array
     */
    private static function getFromIncaper()
    {
        return Cache::remember('municipios', 10, function () {
            $response = Incaper::get('/incaper/api/municipios');

            return array_get(json_decode($response->getBody(), true), 'data', []);
        });
    }

    /**
     * Encontra o município com o código do IBGE.
     *
     * @param int $codigoIbge
     * @return mixed
     */
    public static function getMunicipio($codigoIbge)
    {
        return array_first(static::getFromIncaper(), function ($municpio) use ($codigoIbge) {
            return array_get($municpio, 'codigo') == $codigoIbge;
        });
    }
}
