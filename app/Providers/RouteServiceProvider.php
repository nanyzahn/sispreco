<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapIncaperApiRoutes();

        $this->mapSisprecoApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "incaper api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapIncaperApiRoutes()
    {
        Route::prefix('incaper/api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/incaper_api.php'));
    }

    /**
     * Define the "sispreco api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapSisprecoApiRoutes()
    {
        Route::prefix('sispreco/api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/sispreco_api.php'));
    }
}
