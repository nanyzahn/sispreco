<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Produto extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this['nm_codigo_ibge'],
            'nome' => $this['nm_produto'],
            'unidade_medida' => $this->unidade_medida ? $this->unidade_medida['nm_sigla'] : '',
        ];
    }
}
