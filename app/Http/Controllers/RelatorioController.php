<?php

namespace App\Http\Controllers;

use App\Models\Levantamento;
use App\Models\Municipio;
use App\Models\Produto;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class RelatorioController extends Controller
{
    /**
     * @var array Produtos from the Incaper API.
     */
    public $produtos;

    /**
     * @var array Municípios from the Incaper API.
     */
    public $municipios;

    /**
     * RelatorioController constructor.
     */
    public function __construct()
    {
        $this->produtos = Produto::getAll();
        $this->municipios = Municipio::getAll();
    }

    private function findMunicipioByCodigoIbge($codigoIbge)
    {
        return array_first($this->municipios, function ($municipio) use ($codigoIbge) {
            return array_get($municipio, 'id') == $codigoIbge;
        });
    }

    private function findProdutoByCodigoIbge($codigoIbge)
    {
        return array_first($this->produtos, function ($produto) use ($codigoIbge) {
            return array_get($produto, 'id') == $codigoIbge;
        });
    }

    public function gerarSerieHistorica($request)
    {
        $request->validate([
            'produto' => 'required',
            'municipios' => 'required',
            'ano_inicial' => 'required',
            'ano_final' => 'required',
            'periodo' => 'required',
        ]);

        $produto = $request->get('produto');
        $municipios = $request->get('municipios');
        $anoInicial = $request->get('ano_inicial');
        $anoFinal = $request->get('ano_final');
        $periodo = $request->get('periodo');

        $data = [];

        $produtoModel = $this->findProdutoByCodigoIbge($produto);

        foreach ($municipios as $municipio) {
            $municipioModel = $this->findMunicipioByCodigoIbge($municipio);

            $semanalQuery = 'SELECT ano, mes, semana, AVG(preco) AS preco
                              FROM levantamentos
                              WHERE produto = ? AND municipio = ? AND ano >= ? AND ano <= ?
                              GROUP BY semana, mes, ano
                              ORDER BY ano ASC, mes ASC, semana ASC';

            $mensalQuery = 'SELECT ano, mes, AVG(preco) AS preco
                            FROM (' . $semanalQuery . ') AS mediasemanal
                            GROUP BY mes, ano
                            ORDER BY ano ASC, mes ASC';

            $anualQuery = 'SELECT ano, avg(preco) as preco
                            FROM (' . $mensalQuery . ') AS mediaanual
                            GROUP BY ano
                            ORDER BY ano ASC';

            switch ($periodo) {
                case 'anual':
                    $query = $anualQuery;
                    break;
                case 'mensal':
                    $query = $mensalQuery;
                    break;
                case 'semanal':
                    $query = $semanalQuery;
                    break;
            }

            $resultados = DB::connection('sispreco')->select($query, [$produto, $municipio, $anoInicial, $anoFinal]);

            foreach ($resultados as $result) {
                $item = [
                    'Ano' => $result->ano,
                ];

                if (property_exists($result, 'mes')) {
                    $item['Mês'] = $result->mes;
                }

                if (property_exists($result, 'semana')) {
                    $item['Semana'] = $result->semana;
                }

                $item['Município'] = $municipioModel['nome'];
                $item['Unidade'] = $produtoModel['unidade_medida'];
                $item['Preço'] = 'R$ ' . number_format($result->preco / 100.0, 2, ',', '');

                $data[] = $item;
            };
        }

        return response([
            'error' => false,
            'data' => $data,
        ]);
    }

    public function gerarDetalhada($request)
    {
        $request->validate([
            'produto' => 'required',
            'municipios' => 'required',
            'ano' => 'required',
            'meses' => 'required',
            'semanas' => 'required',
        ]);

        $produto = $request->get('produto');
        $municipios = $request->get('municipios');
        $ano = $request->get('ano');
        $meses = $request->get('meses');
        $semanas = $request->get('semanas');

        $data = [];

        $produtoModel = $this->findProdutoByCodigoIbge($produto);

        foreach ($municipios as $municipio) {
            $municipioModel = $this->findMunicipioByCodigoIbge($municipio);

            $resultados = Levantamento::selectRaw('ano, mes, semana, AVG(preco) as preco')
                ->where('produto', $produto)
                ->where('municipio', $municipio)
                ->where('ano', $ano)
                ->whereIn('mes', $meses)
                ->whereIn('semana', $semanas)
                ->groupBy(['semana', 'mes', 'ano'])
                ->orderBy('ano', 'asc')
                ->orderBy('mes', 'asc')
                ->orderBy('semana', 'asc')
                ->get();

            foreach ($resultados as $result) {
                $item = [
                    'Ano' => $result->ano,
                    'Mês' => $result->mes,
                    'Semana' => $result->semana,
                    'Município' => $municipioModel['nome'],
                    'Unidade' => $produtoModel['unidade_medida'],
                    'Preço' => 'R$ ' . number_format($result->preco / 100.0, 2, ',', ''),
                ];

                $data[] = $item;
            };
        }

        return response([
            'error' => false,
            'data' => $data,
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function gerar(Request $request)
    {
        $request->validate([
            'tipo' => 'required',
        ]);

        $tipo = $request->get('tipo');

        switch ($tipo) {
            case 'serie':
                return $this->gerarSerieHistorica($request);
            case 'detalhada':
                return $this->gerarDetalhada($request);
            default:
                return response([
                    'error' => true,
                    'message' => 'Tipo de relatório inválido.',
                ]);
        }
    }
}
