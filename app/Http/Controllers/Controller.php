<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var array Errors that occurred.
     */
    protected $_errors = [];

    /**
     * Add an error.
     *
     * @param string $error Error to be added.
     * @return Controller Returns a reference to `$this` to allow chaining.
     */
    protected function addError($error = '')
    {
        $this->_errors[] = $error;

        return $this;
    }

    /**
     * Return the errors.
     *
     * @return array
     */
    protected function getErrors()
    {
        return array_values(array_unique($this->_errors));
    }

    /**
     * Check if errors occurred.
     *
     * @return bool
     */
    protected function hasErrors()
    {
        return !empty($this->_errors);
    }
}
